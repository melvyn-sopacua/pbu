SUBDIR_TARGETS+=	deinstall
INSTALLBASE?=		/usr/local
.if defined(FILTERSCRIPTS)
SED?=/usr/bin/sed
FILTERVARS?=INSTALLBASE
SCRIPTS+=${FILTERSCRIPTS}
CLEANFILES+=${FILTERSCRIPTS}
.for script in ${FILTERSCRIPTS}
${script}: ${script}.sh.in var-replace
.endfor
.for _var in ${FILTERVARS}
_FILTER+=-e 's,%%${_var}%%,${${_var}},g'
.endfor
.endif

var-replace:	.USE
	${SED} ${_FILTER} < ${.ALLSRC} > ${.TARGET}

deinstall:
.if defined(PROG) && exists(${DESTDIR}${BINDIR}/${PROG})
	@echo "====> Deinstalling ${DESTDIR}${BINDIR}/${PROG}"
	-@/bin/rm ${DESTDIR}${BINDIR}/${PROG}
.endif
.ifdef SCRIPTS
.	for script in ${SCRIPTS}
	@if [ -f ${DESTDIR}${BINDIR}/${SCRIPTSNAME_${script}} ]; then  \
		echo "====> Deinstalling ${DESTDIR}${BINDIR}/${SCRIPTSNAME_${script}}"; \
		/bin/rm ${DESTDIR}${BINDIR}/${SCRIPTSNAME_${script}}; \
	fi;
.	endfor
.endif

beforeinstall:
	@mkdir -p ${DESTDIR}${BINDIR}
